﻿require("rootpath")();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const basicAuth = require("_helpers/basic-auth");
const errorHandler = require("_helpers/error-handler");
const router = express.Router();
// const swaggerUi = require("swagger-ui-express");
// const swaggerDocument = require("./swagger");

var argv = require("minimist")(process.argv.slice(2));
var swagger = require("swagger-node-express");
// var bodyParser = require( 'body-parser' );

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use basic HTTP auth to secure the api
app.use(basicAuth);

// api routes
app.use("/users", require("./users/users.controller"));
var subpath = express();
app.use(bodyParser());
app.use("/v1", subpath);
swagger.setAppHandler(subpath);
app.use(express.static("dist"));
subpath.get("/", function (req, res) {
  res.sendfile(__dirname + "/dist/index.html");
});

app.use("/v1", subpath);

swagger.setAppHandler(subpath);

swagger.setApiInfo({
  title: "example Express & Swagger API",
  description: "API to do something, manage something...",
  termsOfServiceUrl: "",
  contact: "yourname@something.com",
  license: "",
  licenseUrl: "",
});

swagger.configureSwaggerPaths("", "api-docs", "");

var domain = "localhost";
if (argv.domain !== undefined) domain = argv.domain;
else
  console.log(
    'No --domain=xxx specified, taking default hostname "localhost".'
  );
var applicationUrl = "http://" + domain;
swagger.configure(applicationUrl, "1.0.0");

// app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// global error handler
app.use(errorHandler);
// var applicationUrl = "http://" + domain;
// swagger.configure(applicationUrl, "1.0.0");

// start server
const port = process.env.NODE_ENV === "production" ? 80 : 3000;
const server = app.listen(port, function () {
  console.log("Server listening on port " + port);
});
