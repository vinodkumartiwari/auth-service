﻿// users hardcoded for simplicity, store in a db for production applications
const users = [
  {
    id: 1,
    username: "MTN_user",
    password: "MTN281#^@*",
    firstName: "MTN",
    lastName: "User",
  },
];

module.exports = {
  authenticate,
};

async function authenticate({ username, password }) {
  const user = users.find(
    (u) => u.username === username && u.password === password
  );
  if (user) {
    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword;
  }
}
